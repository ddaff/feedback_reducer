class BaseResponse {
  dynamic data;

  BaseResponse({this.data});

  BaseResponse.fromJson(dynamic item)
      : data = item['Data'];

  Map<String, dynamic> toJson() => {
    'Data': data,
  };
}