import 'package:feedback_reducer/screen/addfeedback/addFeedback-screen.dart';
import 'package:flutter/material.dart';

class ResultFeedbackScreen extends StatefulWidget {
  @override
  _ResultFeedbackScreenState createState() => _ResultFeedbackScreenState();
}

class _ResultFeedbackScreenState extends State<ResultFeedbackScreen> {

  @override
  void initState() {
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Result Feedback",
          style: TextStyle(
            color: Colors.black87
          ),
        ),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        child: Container(
          alignment: Alignment.topCenter,
          margin: EdgeInsets.only(top: 16),
          child: Text("Please press \"add\" button to add feedback"),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        tooltip: "Add feedback",
        onPressed: (){
          showModalBottomSheet(
            context: context,
            builder: (ctx){
              return AddFeedBackScreen();
            }
          );
        },
      ),
    );
  }
}
