import 'package:feedback_reducer/model/client-model.dart';
import 'package:feedback_reducer/model/menu-model.dart';
import 'package:feedback_reducer/model/module-model.dart';
import 'package:feedback_reducer/model/month-model.dart';
import 'package:feedback_reducer/model/taskAction-model.dart';
import 'package:feedback_reducer/model/taskType-model.dart';
import 'package:feedback_reducer/model/year-model.dart';

abstract class AddFeedbackContract {
  showLoading();
  dismissLoading();
  showYear(List<YearModel> year);
  showMonth(List<MonthModel> month);
  showModule(List<ModuleModel> module);
  showMenu(List<MenuModel> menu);
  showTaskType(List<TaskTypeModel> taskType);
  showTaskAction(List<TaskActionModel> taskAction);
  showClient(List<ClientModel> client);
  showPredict(int predict);
  showClassification(int classification);
}