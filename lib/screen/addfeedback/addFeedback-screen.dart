import 'dart:convert';

import 'package:feedback_reducer/model/client-model.dart';
import 'package:feedback_reducer/model/menu-model.dart';
import 'package:feedback_reducer/model/module-model.dart';
import 'package:feedback_reducer/model/month-model.dart';
import 'package:feedback_reducer/model/taskAction-model.dart';
import 'package:feedback_reducer/model/taskType-model.dart';
import 'package:feedback_reducer/model/year-model.dart';
import 'package:feedback_reducer/screen/addfeedback/addFeedback-contract.dart';
import 'package:feedback_reducer/screen/addfeedback/addFeedback-presenter.dart';
import 'package:feedback_reducer/screen/result_screen.dart';
import 'package:flutter/material.dart';

class AddFeedBackScreen extends StatefulWidget {
  @override
  _AddFeedBackScreenState createState() => _AddFeedBackScreenState();
}

class _AddFeedBackScreenState extends State<AddFeedBackScreen> implements AddFeedbackContract {
  AddFeedbackPresenter presenter;
  List<YearModel> years = [];
  List<MonthModel> months = [];
  List<ModuleModel> modules = [];
  List<MenuModel> menus = [];
  List<TaskTypeModel> taskTypes = [];
  List<TaskActionModel> taskActions = [];
  List<ClientModel> clients = [];
  bool loading = false;
  String valYear, valMonth, valModule, valMenu, valTaskType, valTaskAction, valClient;
  int predict, classification;
  DateTime selectedDate = DateTime.now();
  TextEditingController maindays = TextEditingController();

  @override
  void initState() {
    super.initState();
    presenter = AddFeedbackPresenter(this);
    presenter.getYear();
    presenter.getMonth();
    presenter.getModule();
    presenter.getMenu();
    presenter.getTaskType();
    presenter.getTaskAction();
    presenter.getClient();
  }

  selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2000),
        lastDate: DateTime(2025),
    );
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
      });
    }
  }

  sendData(){
    Map<String, String> data = {
      'year' : valYear,
      'month' : valMonth,
      'maindays' : maindays.text,
      'reqDate' : (selectedDate.toLocal()).toString().split(' ')[0],
      'module' : valModule,
      'menu' : valMenu,
      'taskType' : valTaskType,
      'taskAction' : valTaskAction,
      'client' : valClient,
    };
    
    if (data != null) {
      presenter.getPredict().then((value) {
        setState(() {
          predict = value;
          print("Predict : $predict");
        });
        presenter.getClassification().then((value) {
          setState(() {
            classification = value;
            print("Classification : $classification");
          });
          Navigator.push(context, MaterialPageRoute(
              builder: (context) => ResultScreen(result: data, predict: predict, classification: classification,)
          ));
        });
      });
    }else {
      print("null");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Add Feedback",
          style: TextStyle(color: Colors.black87),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      floatingActionButton: FloatingActionButton(
        child: !loading ? Icon(Icons.save) : CircularProgressIndicator(backgroundColor: Colors.white, strokeWidth: 2.0,),
        onPressed: (){
          sendData();
        },
      ),
      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20)
          ),
          height: MediaQuery.of(context).size.height * 0.90,
          margin: EdgeInsets.all(8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Year",
                        style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.45,
                        height: 50,
                        margin: EdgeInsets.symmetric(vertical: 8),
                        padding: EdgeInsets.all(4),
                        decoration: BoxDecoration(
                            border : Border.all(
                              color: Colors.black54,
                              width: 1,
                            ),
                            borderRadius: BorderRadius.circular(8)
                        ),
                        child: DropdownButton(
                          underline: SizedBox(),
                          hint: Text("Select Year"),
                          value: valYear,
                          items: years.map((items) {
                            return
                              DropdownMenuItem(
                              child: Container(
                                  width: MediaQuery.of(context).size.width * 0.35,
                                  child: Text("${items.name}")
                              ),
                              value: items.name,
                            );
                          }).toList(),
                          onChanged: (value) {
                            setState(() {
                              valYear = value;
                            });
                          },
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Month",
                        style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.45,
                        height: 50,
                        margin: EdgeInsets.symmetric(vertical: 8),
                        padding: EdgeInsets.all(4),
                        decoration: BoxDecoration(
                            border : Border.all(
                              color: Colors.black54,
                              width: 1,
                            ),
                            borderRadius: BorderRadius.circular(8)
                        ),
                        child: DropdownButton(
                          underline: SizedBox(),
                          hint: Text("Select Month"),
                          value: valMonth,
                          items: months.map((items) {
                            return DropdownMenuItem(
                              child: Container(
                                  width: MediaQuery.of(context).size.width * 0.35,
                                  child: Text("${items.name}")
                              ),
                              value: items.name,
                            );
                          }).toList(),
                          onChanged: (value) {
                            setState(() {
                              valMonth = value;
                            });
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 8),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Maindays",
                          style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                        ),
                        Container(
                          width: 100,
                          height: 50,
                          margin: EdgeInsets.only(top: 8),
                          child: TextField(
                            controller: maindays,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              labelText: "Maindays",
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Module",
                          style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                        ),
                        Container(
                          width: 140,
                          height: 50,
                          margin: EdgeInsets.only(top: 8),
                          padding: EdgeInsets.all(4),
                          decoration: BoxDecoration(
                              border : Border.all(
                                color: Colors.black54,
                                width: 1,
                              ),
                              borderRadius: BorderRadius.circular(8)
                          ),
                          child: DropdownButton(
                            underline: SizedBox(),
                            hint: Text("Module"),
                            value: valModule,
                            items: modules.map((items) {
                              return
                                DropdownMenuItem(
                                  child: Container(
                                      width: 100,
                                      child: Text("${items.name}")
                                  ),
                                  value: items.name,
                                );
                            }).toList(),
                            onChanged: (value) {
                              setState(() {
                                valModule = value;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Request Date",
                          style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                        ),
                        Container(
                          width: 200,
                          height: 50,
                          margin: EdgeInsets.only(top: 8),
                            padding: EdgeInsets.all(4),
                            decoration: BoxDecoration(
                                border : Border.all(
                                  color: Colors.black54,
                                  width: 1,
                                ),
                                borderRadius: BorderRadius.circular(8)
                            ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                "${selectedDate.toLocal()}".split(' ')[0],
                              ),
                              GestureDetector(
                                onTap: (){
                                  selectDate(context);
                                },
                                child: Icon(Icons.date_range, color: Colors.black54,),
                              )
                            ],
                          )
                        )
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 8),
                child: Text(
                  "Menu",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 50,
                margin: EdgeInsets.symmetric(vertical: 8),
                padding: EdgeInsets.all(4),
                decoration: BoxDecoration(
                    border : Border.all(
                      color: Colors.black54,
                      width: 1,
                    ),
                    borderRadius: BorderRadius.circular(8)
                ),
                child: DropdownButton(
                  underline: SizedBox(),
                  hint: Text("Select Menu"),
                  value: valMenu,
                  items: menus.map((items) {
                    return
                      DropdownMenuItem(
                        child: Container(
                            width: MediaQuery.of(context).size.width * 0.87,
                            child: Text("${items.name}")
                        ),
                        value: items.name,
                      );
                  }).toList(),
                  onChanged: (value) {
                    setState(() {
                      valMenu = value;
                    });
                  },
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * 0.60,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Task",
                          style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width: 140,
                              height: 50,
                              margin: EdgeInsets.only(top: 8),
                              padding: EdgeInsets.all(4),
                              decoration: BoxDecoration(
                                  border : Border.all(
                                    color: Colors.black54,
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.circular(8)
                              ),
                              child: DropdownButton(
                                underline: SizedBox(),
                                hint: Text("Type"),
                                value: valTaskType,
                                items: taskTypes.map((items) {
                                  return
                                    DropdownMenuItem(
                                      child: Container(
                                          width: 100,
                                          child: Text("${items.name}")
                                      ),
                                      value: items.name,
                                    );
                                }).toList(),
                                onChanged: (value) {
                                  setState(() {
                                    valTaskType = value;
                                  });
                                },
                              ),
                            ),
                            Container(
                              width: 140,
                              height: 50,
                              margin: EdgeInsets.only(top: 8),
                              padding: EdgeInsets.all(4),
                              decoration: BoxDecoration(
                                  border : Border.all(
                                    color: Colors.black54,
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.circular(8)
                              ),
                              child: DropdownButton(
                                underline: SizedBox(),
                                hint: Text("Action"),
                                value: valTaskAction,
                                items: taskActions.map((items) {
                                  return
                                    DropdownMenuItem(
                                      child: Container(
                                          width: 100,
                                          child: Text("${items.name}")
                                      ),
                                      value: items.name,
                                    );
                                }).toList(),
                                onChanged: (value) {
                                  setState(() {
                                    valTaskAction = value;
                                  });
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Client",
                          style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                        ),
                        Container(
                          width: 165,
                          height: 50,
                          margin: EdgeInsets.only(top: 8),
                          padding: EdgeInsets.all(4),
                          decoration: BoxDecoration(
                              border : Border.all(
                                color: Colors.black54,
                                width: 1,
                              ),
                              borderRadius: BorderRadius.circular(8)
                          ),
                          child: DropdownButton(
                            underline: SizedBox(),
                            hint: Text("Client"),
                            value: valClient,
                            items: clients.map((items) {
                              return
                                DropdownMenuItem(
                                  child: Container(
                                      width: 120,
                                      child: Text("${items.name}")
                                  ),
                                  value: items.name,
                                );
                            }).toList(),
                            onChanged: (value) {
                              setState(() {
                                valClient = value;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  showYear(List<YearModel> year) {
    setState(() {
      years = year;
    });
  }

  @override
  showMonth(List<MonthModel> month) {
    setState(() {
      months = month;
    });
  }

  @override
  dismissLoading() {
    setState(() {
      loading = false;
    });
  }

  @override
  showLoading() {
    setState(() {
      loading = true;
    });
  }

  @override
  showModule(List<ModuleModel> module) {
    setState(() {
      modules = module;
    });
  }

  @override
  showClient(List<ClientModel> client) {
    setState(() {
      clients = client;
    });
  }

  @override
  showMenu(List<MenuModel> menu){
    setState(() {
      menus = menu;
    });
  }

  @override
  showTaskAction(List<TaskActionModel> taskAction) {
    setState(() {
      taskActions = taskAction;
    });
  }

  @override
  showTaskType(List<TaskTypeModel> taskType) {
    setState(() {
      taskTypes = taskType;
    });
  }

  @override
  showClassification(int classification) {

  }

  @override
  showPredict(int predict) {

  }
}
