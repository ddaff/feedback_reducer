import 'package:feedback_reducer/base/base-repository.dart';
import 'package:feedback_reducer/model/client-model.dart';
import 'package:feedback_reducer/model/menu-model.dart';
import 'package:feedback_reducer/model/module-model.dart';
import 'package:feedback_reducer/model/month-model.dart';
import 'package:feedback_reducer/model/taskAction-model.dart';
import 'package:feedback_reducer/model/taskType-model.dart';
import 'package:feedback_reducer/model/year-model.dart';
import 'package:feedback_reducer/screen/addfeedback/addFeedback-contract.dart';

class AddFeedbackPresenter {
  BaseRepository repos = BaseRepository();
  AddFeedbackContract view;
  AddFeedbackPresenter(this.view);

  getYear(){
    view.showLoading();
    repos.fetch("year.json", RequestType.get).then((res) {
      List<YearModel> yearResponse = (res as List).map((year) =>
          YearModel.fromJson(year)
      ).toList();
      view.showYear(yearResponse);
      view.dismissLoading();
    }).catchError((error){
      view.dismissLoading();
      print("Error : $error");
    });
  }

  getMonth(){
    view.showLoading();
    repos.fetch("month.json", RequestType.get).then((res) {
      List<MonthModel> monthResponse = (res as List).map((month) =>
          MonthModel.fromJson(month)
      ).toList();
      view.showMonth(monthResponse);
      view.dismissLoading();
    }).catchError((error){
      view.dismissLoading();
      print("Error : $error");
    });
  }

  getModule(){
    view.showLoading();
    repos.fetch("module.json", RequestType.get).then((res) {
      List<ModuleModel> moduleResponse = (res as List).map((module) =>
          ModuleModel.fromJson(module)
      ).toList();
      view.showModule(moduleResponse);
      view.dismissLoading();
    }).catchError((error){
      print("Error : $error");
    });
  }

  getMenu(){
    view.showLoading();
    repos.fetch("menu.json", RequestType.get).then((res) {
      List<MenuModel> menuResponse = (res as List).map((menu) =>
          MenuModel.fromJson(menu)
      ).toList();
      view.showMenu(menuResponse);
      view.dismissLoading();
   }).catchError((error){
     view.dismissLoading();
     print("Error : $error");
    });
  }

  getTaskType(){
    view.showLoading();
    repos.fetch("tasktype.json", RequestType.get).then((res){
      List<TaskTypeModel> taskTypeResponse = (res as List).map((taskType) =>
          TaskTypeModel.fromJson(taskType)
      ).toList();
      view.showTaskType(taskTypeResponse);
      view.dismissLoading();
    }).catchError((error){
      view.dismissLoading();
      print("Error : $error");
    });
  }

  getTaskAction(){
    view.showLoading();
    repos.fetch("taskaction.json", RequestType.get).then((res){
      List<TaskActionModel> taskActionResponse = (res as List).map((taskAction) =>
          TaskActionModel.fromJson(taskAction)
      ).toList();
      view.showTaskAction(taskActionResponse);
      view.dismissLoading();
    }).catchError((error){
      view.dismissLoading();
      print("Error : $error");
    });
  }

  getClient(){
    view.showLoading();
    repos.fetch("client.json", RequestType.get).then((res) {
      List<ClientModel> clientResponse = (res as List).map((client) =>
          ClientModel.fromJson(client)
      ).toList();
      view.showClient(clientResponse);
      view.dismissLoading();
    }).catchError((error){
      view.dismissLoading();
      print("Error : $error");
    });
  }

 Future<dynamic> getPredict(){
    view.showLoading();
    return repos.fetchCustom("predict.json", RequestType.get).then((res) {
      view.dismissLoading();
      return res;
    }).catchError((error){
      view.dismissLoading();
      print("Error : $error");
    });
  }

  Future<dynamic> getClassification(){
    view.showLoading();
    return repos.fetchCustom("classification.json", RequestType.get).then((res) {
      view.dismissLoading();
      return res;
    }).catchError((error){
      view.dismissLoading();
      print("Error : $error");
    });
  }
}