import 'package:flutter/material.dart';

class ResultScreen extends StatelessWidget {
  final Map<String, String> result;
  final int predict;
  final int classification;

  ResultScreen({this.result, this.predict, this.classification});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "Result",
            style: TextStyle(color: Colors.black87),
          ),
          leading: GestureDetector(
            child: Icon(Icons.arrow_back, color: Colors.black,),
            onTap: (){
              Navigator.pop(context);
            },
          ),
          backgroundColor: Colors.white,
          elevation: 0,
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Container(
                        child: Text("Year",
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    )),
                    SizedBox(
                      width: 24,
                    ),
                    Container(
                        child: Text("${result['year']}",
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    )),
                  ],
                ),
                SizedBox(height: 16,),
                Row(
                  children: [
                    Container(
                        child: Text("Month",
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    )),
                    SizedBox(
                      width: 24,
                    ),
                    Container(
                        child: Text("${result['month']}",
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    )),
                  ],
                ),
                SizedBox(height: 16,),
                Row(
                  children: [
                    Container(
                        child: Text("Maindays",
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    )),
                    SizedBox(
                      width: 24,
                    ),
                    Container(
                        child: Text("${result['maindays']} day(s)",
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    )),
                  ],
                ),
                SizedBox(height: 16,),
                Row(
                  children: [
                    Container(
                        child: Text("Request Date",
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    )),
                    SizedBox(
                      width: 24,
                    ),
                    Container(
                        child: Text("${result['reqDate']}",
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    )),
                  ],
                ),
                SizedBox(height: 16,),
                Row(
                  children: [
                    Container(
                        child: Text("Menu",
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    )),
                    SizedBox(
                      width: 24,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.70,
                        child: Text("${result['menu']}",
                      softWrap: true,
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    )),
                  ],
                ),
                SizedBox(height: 16,),
                Row(
                  children: [
                    Container(
                        child: Text("Module",
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    )),
                    SizedBox(
                      width: 24,
                    ),
                    Container(
                        child: Text("${result['module']}",
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    )),
                  ],
                ),
                SizedBox(height: 16,),
                Row(
                  children: [
                    Container(
                        child: Text("Client",
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    )),
                    SizedBox(
                      width: 24,
                    ),
                    Container(
                        child: Text("${result['client']}",
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    )),
                  ],
                ),
                SizedBox(height: 16,),
                Row(
                  children: [
                    Container(
                        child: Text("Task Type",
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    )),
                    SizedBox(
                      width: 24,
                    ),
                    Container(
                        child: Text("${result['taskType']}",
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    )),
                  ],
                ),
                SizedBox(height: 16,),
                Row(
                  children: [
                    Container(
                        child: Text("TaskAction",
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    )),
                    SizedBox(
                      width: 24,
                    ),
                    Container(
                        child: Text("${result['taskAction']}",
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    )),
                  ],
                ),
                SizedBox(height: 16,),
                Row(
                  children: [
                    Container(
                        child: Text("Predict",
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    )),
                    SizedBox(
                      width: 24,
                    ),
                    Container(
                        child: Text("$predict",
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    )),
                  ],
                ),
                SizedBox(height: 16,),
                Row(
                  children: [
                    Container(
                        child: Text("Classification",
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    )),
                    SizedBox(
                      width: 24,
                    ),
                    Container(
                        child: Text("$classification",
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    )),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
