class TaskTypeModel {
  String code;
  String name;

  TaskTypeModel({this.code, this.name});

  TaskTypeModel.fromJson(Map<String, dynamic> json) {
    code = json['Code'];
    name = json['Name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Code'] = this.code;
    data['Name'] = this.name;
    return data;
  }
}